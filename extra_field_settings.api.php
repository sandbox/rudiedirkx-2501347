<?php

/**
 * Step 1: define your extra fields.
 *
 * Implements hook_field_extra_fields().
 */
function extra_field_settings_field_extra_fields() {
  $fields['node']['page']['display']['temperature'] = array(
    'label' => t('Temperature'),
    'description' => t('Temperature'),
    'weight' => -1,

    // New: add Label dropdown, like with real fields. To be used manually by extra field owner!
    // Only for 'display' extra fields, not 'form'.
    'configurable_label' => TRUE,

    // New: default settings.
    'settings' => array(
      'label_display' => 'inline', // This one's handled by me, but you can add a default.
      'suffix' => 'K',
    ),
  );
  return $fields;
}

/**
 * Step 2: add settings to those fields.
 *
 * Implements hook_extra_field_settings_form_alter().
 *
 * $context contains
 * - $entity_type
 * - $bundle
 * - $extra_field_name
 * - $ebf => $entity_type . ':' . $bundle . ':' . $name
 * - $instance
 * - $view_mode
 * - $settings
 * - &$form_state
 */
function extra_field_settings_extra_field_settings_form_alter(array &$form, array $context) {
  if ($context['ebf'] == 'node:page:temperature') {
    $settings = $context['settings'];
    $suffix = @$settings['suffix'] ?: '';
    $form['suffix'] = array(
      '#type' => 'textfield',
      '#title' => t('Suffix, like &quot;F&quot; or &quot;C&quot;'),
      '#default_value' => $suffix,
    );
  }
}

/**
 * Step 3: summarize those settings.
 *
 * Implements hook_extra_field_settings_summary_alter().
 *
 * $context contains
 * - $entity_type
 * - $bundle
 * - $extra_field_name
 * - $ebf => $entity_type . ':' . $bundle . ':' . $name
 * - $instance
 * - $view_mode
 * - $settings
 * - &$form_state
 */
function extra_field_settings_extra_field_settings_summary_alter(array &$summary, array $context) {
  if ($context['ebf'] == 'node:page:temperature') {
    $settings = $context['settings'];
    $suffix = @$settings['suffix'] ?: '';
    $summary['suffix'] = array(
      '#type' => 'item',
      '#markup' => t('Suffix: &quot;@suffix&quot;', array('@suffix' => $suffix)),
    );
  }
}

/**
 * Step 4: use those settings when building.
 *
 * Implements hook_node_view().
 */
function extra_field_settings_node_view(object $node, string $view_mode, string $langcode) {
  if ($node->type == 'page') {
    $settings = extra_field_settings_get('node', 'page', $view_mode, 'temperature');
    $suffix = @$settings['suffix'] ?: '';

    $content = rand(-40, 100) . $suffix;
    $node->content['temperature'] = '<div class="temperature">' . $content . '</div>';
  }
}
